package dlemontagner.circuitbreakerdemoclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CircuitbreakerDemoClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(CircuitbreakerDemoClientApplication.class, args);
	}

}
