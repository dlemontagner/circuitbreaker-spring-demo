package dlemontagner.circuitbreakerdemoclient;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class CircuitBreakerDemoClientController {
    CircuitBreakerDemoClientService circuitBreakerDemoClientService;

    public CircuitBreakerDemoClientController(CircuitBreakerDemoClientService circuitBreakerDemoClientService){
        this.circuitBreakerDemoClientService = circuitBreakerDemoClientService;
    }
    @GetMapping
    public Mono<String> HelloWorld(){
        return circuitBreakerDemoClientService.getHelloWorld();
    }

}
