package dlemontagner.circuitbreakerdemoclient;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
public class CircuitBreakerDemoClientService {
    private final WebClient webClient;
    private final static String HELLOWORLD_PATH = "/";
    private final static String HELLOWORLD_FALLBACK_RESULT = "Hello Fallback!";

    public CircuitBreakerDemoClientService(WebClient.Builder webClientBuilder, @Value("${helloWorld.base-uri}") String helloWorldBaseUri) {
        this.webClient = webClientBuilder.baseUrl(helloWorldBaseUri).build();
    }

    @CircuitBreaker(name = "helloWorld", fallbackMethod = "getHelloWorld_fallback")
    public Mono<String> getHelloWorld() {
        return this.webClient.get().uri(HELLOWORLD_PATH)
                .retrieve().bodyToMono(String.class);
    }

    public Mono<String> getHelloWorld_fallback(Throwable ex) {
        return Mono.just(HELLOWORLD_FALLBACK_RESULT);
    }

}
