package dlemontagner.circuitbreakerdemoclient;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.RepetitionInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;

import java.util.concurrent.TimeUnit;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(
    webEnvironment = RANDOM_PORT,
    properties = {
            "helloWorld.base-uri=http://localhost:${wiremock.server.port}"
    }
)
@AutoConfigureWireMock(port = 0)
public class CircuitBreakerDemoClientServiceTests {
    private static final int HELLOWORLD_SERVICE_OUTAGE_START = 2;
    private static final int HELLOWORLD_SERVICE_OUTAGE_END = 11;
    private static final int CIRCUITBREAKER_OPEN_START = 11;
    private static final int CIRCUITBREAKER_OPEN_END = 13;
    private static final int HALF_OPEN_STATE_START = 14;
    private static final int HALF_OPEN_STATE_WAIT_DURATION = 5;
    private static final String HELLOWORLD_URL_PATH = "/";
    private static final String HELLOWORLD_SUCCESSFUL_RESULT = "Hello World!";
    private static final String HELLOWORLD_FALLBACK_RESULT = "Hello Fallback!";

    @Autowired
    CircuitBreakerDemoClientService circuitBreakerDemoClientService;

    @Autowired
    private WireMockServer HELLO_WORLD_MOCKED_SERVICE;

    @RepeatedTest(20)
    public void testCircuitBreaker(RepetitionInfo repetitionInfo) throws InterruptedException {
        int currentRepetition = repetitionInfo.getCurrentRepetition();

        waitForHalfOpenStateIfNeeded(currentRepetition);
        mockServiceBasedOnIteration(currentRepetition);

        String expectedHelloWorldResult = getExpectedHelloWorldResult(currentRepetition);
        int expectedRequestCount = getExpectedRequestCount(currentRepetition);

        String helloWorldResult = circuitBreakerDemoClientService.getHelloWorld().block();

        assertThat(helloWorldResult).isEqualTo(expectedHelloWorldResult);
        HELLO_WORLD_MOCKED_SERVICE.verify(expectedRequestCount, getRequestedFor(urlEqualTo(HELLOWORLD_URL_PATH)));
    }

    private void waitForHalfOpenStateIfNeeded(int currentRepetition) throws InterruptedException {
        // Before starting iteration 14, wait 5 seconds
        // So that circuit breaker has time to switch to half open state
        if(currentRepetition == HALF_OPEN_STATE_START)
            TimeUnit.SECONDS.sleep(HALF_OPEN_STATE_WAIT_DURATION);
    }

    private void mockServiceBasedOnIteration(int currentRepetition) {
        HELLO_WORLD_MOCKED_SERVICE.resetAll();

        // For iterations between 2 and 11 included, helloWorld service is not working
        if(currentRepetition >= HELLOWORLD_SERVICE_OUTAGE_START && currentRepetition <= HELLOWORLD_SERVICE_OUTAGE_END)
            HELLO_WORLD_MOCKED_SERVICE.stubFor(WireMock.get(HELLOWORLD_URL_PATH)
                    .willReturn(serverError()));
        else
            // First iteration and after iteration 12, helloWorld service is working
            HELLO_WORLD_MOCKED_SERVICE.stubFor(WireMock.get(HELLOWORLD_URL_PATH)
                    .willReturn(aResponse()
                            .withBody(HELLOWORLD_SUCCESSFUL_RESULT)));
    }

    private String getExpectedHelloWorldResult(int currentRepetition) {
        // Fallback response is expected when service is not working (iterations 2 to 11)
        // or circuit Breaker is open (iterations 11, 12, 13)
        if(currentRepetition >= HELLOWORLD_SERVICE_OUTAGE_START && currentRepetition <= CIRCUITBREAKER_OPEN_END)
            return HELLOWORLD_FALLBACK_RESULT;

        return HELLOWORLD_SUCCESSFUL_RESULT;
    }

    private int getExpectedRequestCount(int currentRepetition) {
        // After 10 calls, if failure rate is above 50%, circuit breaker is triggered
        // Meaning the requests should NOT reach the hello world service
        // As we'll wait 5 seconds between repetition 13 & 14,
        // requests should reach back hello world service after repetition 13
        if(currentRepetition >= CIRCUITBREAKER_OPEN_START && currentRepetition <= CIRCUITBREAKER_OPEN_END)
            return 0;

        return 1;
    }

}
