package dlemontagner.circuitbreakerdemohelloworld;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class CircuitbreakerDemoHelloworldApplication {

	public static void main(String[] args) {
		SpringApplication.run(CircuitbreakerDemoHelloworldApplication.class, args);
	}

	@RestController
	class HelloController {
		@GetMapping
		public String helloWorld() {
			return "Hello World!";
		}
	}
}
