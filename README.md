# Circuit Breaker Spring Boot / Resilience4J Demo

Ce code source est un exemple accompagnant [mon article](https://david.le-montagner.fr/posts/renforcer-la-resilience-grace-au-circuit-breaker/) à propos du pattern Circuit Breaker et de son implémentation via Spring Boot et Resilience4J.

Bien que ça ne soit pas l'objet de l'article, il y a également les élèments nécessaires au démarrage d'une stack grafana/prometheus de façon à consulter les éléments d'observabilité.

## Lancement du service "helloworld"

```shell script
mvn -f circuitbreaker-demo-helloworld/pom.xml spring-boot:run
```

## Lancement du service "client"

```shell script
mvn -f circuitbreaker-demo-client/pom.xml spring-boot:run
```

## Lancement de la stack d'observabilité

```shell script
docker compose -f circuitbreaker-observability-stack/docker-compose.yml up
```

Se connecter à Grafana via l'url http://localhost:3000/login

Login/password par défaut: admin/admin

Cliquez sur le menu en haut à gauche et choisir "Dashboards"

Sous la zone "Applications", vous pouvez choisir "Circuit Breakers" et observer les données envoyées par le service "client".
